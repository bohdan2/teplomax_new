$(function() {


	$(".button").mPageScroll2id();
	$(window).scroll(function(){

		var st = $(this).scrollTop();

		if(st >= 200){
			$('.header_bottom_left_trien_wrap').css('display', 'block');
			$('.button').removeClass('hidden');
		}else if(st < 200){
			$('.header_bottom_left_trien_wrap').css('display', 'none');
			$('.button').addClass('hidden');
		}

	});

	$('.carousel_sec_2').owlCarousel({
		items:1,
		dotData:true,
		stagePadding:50,
		margin:90,
		smartSpeed:450,
		nav:false,
		mouseDrag:false,
		responsive:{
        	0:{
        	    items:1
        	}       	
    }	
	});

	$('.carousel_2_inside').owlCarousel({
		items:5,
		loop:true,
		margin:40,
		nav:true,
		navText: ['<img src="../img/slide_left.png" alt="" />', '<img src="../img/slide_right.png" alt="" />']
	});

	$('.carousel_2_inside > .owl-nav').removeClass('disabled');


	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ['<img src="img/arrow_left.png" alt="" />', '<img src="img/arrow_right.png" alt="" />'],
    responsive:{
        0:{
            items:1
        },
        321:{
            items:2
        },
        600:{
            items:2
        },
        992:{
            items:3
        },
        1200:{
            items:4
        }
    }
})

	var clock;
	clock = $('.clock').FlipClock({
		clockFace: "DailyCounter",
		autoStart: true,

	})

	clock.setTime(5000);
	clock.start();


	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});

});
